
task main()
{
	//Reset the motor encoder value to zero
	resetMotorEncoder(motorB);
	resetMotorEncoder(motorC);
	
	//Set the target of motor B to 360 encoder counts at speed 50
	setMotorTarget(motorB, 360, 50);
	setMotorTarget(motorC, 180, 50);
	
	while (getMotorEncoder(motorB) < 360) {
		// Display the value of the encoder on the LCD display
		displayCenteredBigTextLine(4,"EncoderB: %3d", getMotorEncoder(motorB));
		displayCenteredBigTextLine(6,"EncoderC: %3d", getMotorEncoder(motorC));
		
	}
	
}
